with open("input11.txt") as f:
    xs=f.read().strip().split("\n")
xs=[ [x for x in y] for y in xs]
def count_seats(y,x,s,xs):
    if y-1>=0 and xs[y-1][x]=="#":
        s+=1
    if y-1>=0 and x+1<len(xs[0]) and xs[y-1][x+1]=="#":
        s+=1
    if y-1>=0 and x-1>0 and xs[y-1][x-1]=="#":
        s+=1
    if y+1<len(xs) and xs[y+1][x]=="#":
        s+=1
    if y+1<len(xs) and x+1<len(xs[0]) and xs[y+1][x+1]=="#":
        s+=1
    if y+1<len(xs) and x-1>=0 and xs[y+1][x-1]=="#":
        s+=1
    if x-1>=0 and xs[y][x-1]=="#":
        s+=1
    if x+1<len(xs[0]) and xs[y][x+1]=="#":
        s+=1
    return s
lst=[[ x for x in y] for y in xs]
c=0
for j in range(100000):
    x=0
    y=0
    for i in xs:
        for z in i:
            if xs[y][x]!=".":
                if xs[y][x]=="L" and count_seats(y,x,0,xs)==0:
                    lst[y][x]="#"
                    if x+1<len(xs[0]):
                        x+=1
                    elif x+1==len(xs[0]):
                        x=0
                        y+=1
                elif xs[y][x]=="#" and count_seats(y,x,0,xs)>=4:
                    lst[y][x]="L"
                    if x+1<len(xs[0]):
                        x+=1
                    elif x+1==len(xs[0]):
                        x=0
                        y+=1
            else:
                if x+1<len(xs[0]):
                    x+=1
                elif x+1==len(xs[0]):
                    x=0
                    y+=1
    if lst==xs:
        summ=0
        for x in lst:
            for i in x:
                if i=="#":
                    summ+=1
        print(summ)
        break
    
    xs=[[ x for x in y] for y in lst]
    c+=1
    

