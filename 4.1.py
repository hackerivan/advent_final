with open("4-1input.txt") as f:
    content=f.read().strip()
content=content.split("\n\n")
print(content[0])
content=[x.replace("\n" , " ") for x in content]
content=[x.split(" ") for x in content]


passports=[]
for passport in content:
    data=[]
    for x in passport:
       data.append(x.split(":"))
    passports.append(dict(data))
print(passports[:10])    
s=0
valid={"byr","iyr","eyr","hgt","hcl","ecl","pid"}
for passport in passports:
    if set(passport.keys()).issuperset(valid):
        s=s+1
print(s)

valid=0
for passport in passports:
    s=0
    data=set(passport.keys())
    if "byr" in data and 1920<=int(passport["byr"])<=2002:
        s=s+1
    if "iyr" in data and 2010<=int(passport["iyr"])<=2020:
        s=s+1
    if "eyr" in data and 2020<=int(passport["eyr"])<=2030:
        s=s+1
    if "hgt" in data and (passport["hgt"][-2:]=="cm" or passport["hgt"][-2:]=="in"):
        if passport["hgt"][-2:]=="cm":
            if 150<=int(passport["hgt"][:-2])<=193:
                s=s+1
        elif passport["hgt"][-2:]=="in":
            if 59<=int(passport["hgt"][:-2])<=76:
                s=s+1
    if "hcl" in data:
        b=0
        if passport["hcl"][0]=="#":
            a=passport["hcl"][1:]
            if len(a)==6:
                for c in a:
                    if "0"<=c<="9" or "a"<=c<="f":
                        b=b+1
                if b==6:
                    s=s+1
    ecl=["amb","blu","brn","gry","grn","hzl","oth"]
    if "ecl" in data and passport["ecl"] in ecl:
        s=s+1
    if "pid" in data:
        a=passport["pid"]
        b=0
        for c in a:
            if len(a)==9:
                for c in a:
                    if "0"<=c<="9":
                        b=b+1
                if b==9:
                    s=s+1
    if s==7:
        valid+=1
print(valid)
        
                    
                

















        
