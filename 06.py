with open("input06.txt") as f:
    xs=f.read().strip()
xs=xs.split("\n\n")
xs=[x.split("\n") for x in xs]
print(xs[0])
ys=set()
# solution with set
s=0
for x in xs:
    for y in x:
        for z in y:
            ys.add(z)
    s=s+len(ys)
    ys=set()
print(s)
# solution with intersection
s=0
for x in xs:
    f=set(x[0])
    for i in range(1,len(x)):
        y=set(x[i])
        f=f.intersection(y)
    s=s+len(f)
print(s)



# first solution without sets
for x in xs:
    a=set()
    for y in x:
        for i in range(len(y)):
            a.add(y[i])
    ys.append(len(a))
s=1
print(sum(ys))
    
with open("input06.txt") as f:
    xs=f.read().strip()
xs=xs.split("\n\n")
xs=[x.split("\n") for x in xs]
ys=[]
for x in xs:
    s=0
    a=dict()
    ln=len(x)
    for y in x:
        for i in range(len(y)):
            if y[i] not in a:
                a[y[i]]=1
            else:
                a[y[i]]+=1

    for value in a.values():
        if value == ln:
            s=s+1
    ys.append(s)
    
print(sum(ys))

