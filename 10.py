with open("input10.txt") as f:
    xs=f.read().strip().split("\n")
xs=[int(x) for x in xs]
xs.append(0)
xs.append(max(xs)+3)
xs=sorted(xs)
s1=0
s3=0
ys=[]
for i,j in zip(xs,xs[1:]):
    if j-i==3:
        s3+=1
    if j-i==1:
        s1+=1
    ys.append(j-i)
print(s1*s3)

s=0
lst=[]
for i in ys:
    if i==1:
        s+=1
    else:
      lst.append(s)
      s=0
print(lst)
g=1
for i in lst:
    if i==1:
        g=g*1
    if i==2:
        g=g*2
    if i==3:
        g=g*4
    if i==4:
        g=g*7
print(g)
        
